import json
import sys
import os

p = {
    # Specify the type of task by the type of label
    # Mask, Blob, Disk, Gaussian, Exitwave
    'label': "Disk", 

    # Beam energy in eV
    'beamenergy': 50e3,
    
    # Number of images (at different microscope parameters) per exit wave.
    'image_epochs': 10,
    
    # Size of the images during training (x, y)
    'image_size': (640, 640),

    # Size of wave function arrays (x, y)
    'wave_size': (1600, 1600),

    # Number of classes in output of network, including the background
    # class.  Setting num_classes=1 means just a single class, no
    # background.  Otherwise, num_classes should be one higher than
    # the number of actual classes, to make room for the background
    # class.
    'num_classes': 3,
    'null_class' : True,
    
    # Spot size of atomic columns in Angstrom
    'spotsize': 0.4,

    # Image resolution range in pixels/Angstrom
    'sampling': (0.1, 0.11),

    # Sampling of wave function
    'wave_sampling': 0.05,

    # Slice thickness for the multislice algorithm
    'multislicethickness': 0.25,

    # Focal series if not None.
    # (number of images, change in focus, random part of change).
    'multifocus': (3, 50.0, 1.0),   

    ## Noise parameters 
    # Range of the logarithm (base 10) of the dose in electrons/A^2
    'log_dose': (2.5, 5),
    
    # Range of blur
    'blur': (0.05, 0.25),

    # Range of focal spread
    'focal_spread': (5, 10),

    ## CTF Parameters (values from microscope at Nanolab)
    # Defocus range in Angstrom
    'defocus': (-150, 50),
    # Spherical abberation in Angstrom (1 micrometer = 1e4 A)
    'ctf_c30': (-15e4, 15e4),
    # 2-fold astigmatism in Angstrom
    'ctf_c12': (0, 25),
    # Axial coma in Angstrom
    'ctf_c21': (0, 600),
    # 3-fold astigmatism in Angstrom
    'ctf_c23': (0, 0),
    # Star abberation in Angstrom
    'ctf_c32': (0, 0),
    # 4-fold astigmatism in Angstrom
    'ctf_c34': (0, 0),
    # 5-fold astigmatism in Angstrom
    'ctf_c45': (0, 0),

    ## MTF Parameters
    # Range of MTF parameters (same names as in paper, except c0 which is 1.0 in paper).
    #'mtf_c0': (0.12, 0.16),
    "mtf": "lorentzian",
    'mtf_c1': (0.2, 0.6),
    'mtf_c2': (0.0, 0.0),
    #'mtf_c3': (2.0, 5.0),

    ## Readout noise
    'readout': (0.005, 0.015),

    # normalization distance in Angstrom
    'normalizedistance': 12.0,

    # How many images to save in debug folder (None=none, True=all).
    'debug': 100,
    
    # If a seed to the random number generator should be used
    # Can be set to True to generate a random seed that is reused in testing run
    # or set to a specific seed that is used for reproducible training.
    # Setting it to None disable deterministic testing as well.
    #'seed': [998616271, 738814483, 960996334, 235280404, 560394456],
    
    'Phonon_Debye_Temp': 580,
    
    'Phonon_Temp': (100, 600),
    
    'Frozen_Phonon_Configs': 10,
    
    'Phonon': 'Frozen'
}

if __name__ == '__main__':
    thisname = sys.argv[0]
    basename = os.path.basename(thisname)
    if basename.endswith('.py'):
        outname = basename[:-3] + '.json'
    else:
        raise RuntimeError(f'Cannot work out json file name from script name {thisname}')
    print(f'Writing parameters: {basename} -> {outname}')
    with open(outname, 'w') as json_file:
        json.dump(p, json_file, sort_keys=True, indent=4)

#!/bin/bash
#SBATCH --job-name=MoS2-images-layers-8
#SBATCH --mail-type=START,END
#SBATCH --partition=xeon40
#SBATCH --output=slurm-%x-%j.out
#SBATCH --time=24:00:00
#SBATCH -N 1
#SBATCH -n 40

# Necessary modules
source $HOME/bachelor/bachelor-venv/bin/activate

export OMP_NUM_THREADS=1

python make_image_data.py ../simulation_data/MoS2_supported_sample_layers_8 parameters_exitwave.json -n 30 --train
python make_image_data.py ../simulation_data/MoS2_supported_sample_layers_8 -n 30 --test

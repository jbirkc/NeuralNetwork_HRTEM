#!/usr/bin/env python
# coding: utf-8
import sys
sys.path.insert(0, '../../')

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from glob import glob
import numpy as np
# Peak detection
from scipy.spatial import cKDTree as KDTree
import sys
import os
import tempfile
import hyperspy.api as hs
import skimage.io
from stm.feature.peaks import find_local_peaks, refine_peaks
from skimage.morphology import disk
from scipy.spatial import cKDTree as KDTree
from stm.preprocess import normalize
import argparse

def namegenerator(imgroot, dataroot, predroot, filetype):
    """Generate input and output file names.

    Given the folder names, this generator makes matching input and
    output file paths, replicating the full folder tree of the input
    folder in the output folders.
    """
    for root, dirs, files in os.walk(imgroot):
        dirs.sort()
        files.sort()
        for f in files:
            fullname = os.path.join(root, f)
            f_noext, ext = os.path.splitext(fullname)
            if ext not in [filetype]:
                continue
            assert f_noext.startswith(imgroot)
            dataname = dataroot + f_noext[len(imgroot):] + '_peaks.npy'
            predname = predroot + f_noext[len(imgroot):] + '_inference.npz'
            yield fullname, dataname, predname

def get_image(f):
    """Load an image, return data as a numpy array."""
    if f.endswith('.dm4'):   # Extend with .dm3 also?
        a = hs.load(f)
        image = a.data
    else:
        a = skimage.io.imread(f)
        image = skimage.color.rgb2gray(a)
    return image

def get_points(f):
    atoms = np.load(f)
    return atoms

def get_prediction(f):
    pred = np.load(f)['prediction']
    return pred

def crop(image1, image2, atoms, xmin=0, xmax=None, ymin=0, ymax=None):
    shape = image1.shape
    if xmax is None:
        xmax = shape[0]
    if ymax is None:
        ymax = shape[1]
    image1 = image1[xmin:xmax,ymin:ymax]
    if image2 is not None:
        image2 = image2[xmin:xmax,ymin:ymax]
    d = 7
    atoms = atoms[np.where(atoms[:,0] > xmin + d)]
    atoms = atoms[np.where(atoms[:,1] > ymin + d)]
    atoms = atoms[np.where(atoms[:,0] < xmax - d)]
    atoms = atoms[np.where(atoms[:,1] < ymax - d)]
    atoms = atoms - np.array((xmin, ymin))
    return image1, image2, atoms

# In[ ]:

def find_peaks_again(prediction):
    distance = int(2.5 / sampling)
    peaks = find_local_peaks(prediction, min_distance=distance, 
                             threshold=threshold, exclude_border=10,
                             exclude_adjacent=True)
    peaks = refine_peaks(normalize(prediction), peaks, 
                         disk(2), model='polynomial')
    return peaks

# In[ ]:

def makefigure(image, atoms, prediction, plotfile=None):
    #image, prediction, atoms = crop(image, prediction, atoms, xmin=500, xmax=1150, ymin=150, ymax=800)

    fig, (ax_raw, ax_pred, ax_atoms) = plt.subplots(1,3,figsize=(20,11))
    #fig, (ax_raw, ax_pred, ax_atoms) = plt.subplots(3,1,figsize=(12,40))

    ax_raw.imshow(image,cmap='gray')
    ax_pred.imshow(prediction,cmap='gray')
    ax_atoms.imshow(image, cmap='gray')
    #mycolor = '#80FFFF'
    mycolor = '#FFFF00'
    ax_atoms.scatter(atoms[:,1], atoms[:,0], c=mycolor, marker='o', linewidth=2.0)
    ax_raw.axis('off')
    ax_pred.axis('off')
    ax_atoms.axis('off')
    plt.tight_layout()
    if plotfile:
        fig.savefig(plotfile, bbox_inches='tight')
        plt.close(fig)
# ## Run this section only for draft

# In[ ]:

if False:
    fi, fd, fp = next(names)
    print(fi)
    image = get_image(fi)
    atoms = get_points(fd)
    prediction = get_prediction(fp)
    makefigure(image, atoms, prediction)

# Parse the command line
parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument("datafolder",
                    help="The path and name of the folder where the experimental data is placed.")
parser.add_argument("--filetype",
                    help="The file extension of the experimental data (.dm4, .dm3, .png).",
                    default='.dm4')
parser.add_argument("--threshold",
                    help="Threshold for detecting atoms in CNN output (default: Don't reanalyze)",
                    default=None, type=float)
parser.add_argument("--sampling",
                    help="Sampling in Angstrom/pixel (not critical, default: 0.1 Å/pixel)",
                    default=0.1, type=float)
args = parser.parse_args()

imgroot = args.datafolder
# Remove trailing path separator (slash on unix, backslash on windows)
if not os.path.split(imgroot)[1]:
    imgroot = os.path.split(imgroot)[0]
assert not imgroot.endswith('/')
threshold = args.threshold
sampling = args.sampling

atomsroot = imgroot + '_peaks'
predroot = imgroot + "_inference"
videoroot = imgroot + "_video"
scriptname = os.path.join(videoroot, "convertmovie.sh")
moviename = os.path.split(imgroot)[1] + ".m4v"

print("Reading images from folder {}".format(imgroot))
print("Output folder:", videoroot)
print("   Script file:", scriptname)
print("   Video file:", moviename)

# Produce the .png files for the movie
names = namegenerator(imgroot, atomsroot, predroot, filetype=args.filetype)

if not os.path.exists(videoroot):
    os.mkdir(videoroot)
    
filenames = os.path.join(videoroot, 'files.txt')
shortfilenames = os.path.join(videoroot, 'files-short.txt')
with open(filenames, "wt") as f, open(shortfilenames, "wt") as sf:
    for i, (fi, fd, fp) in enumerate(names):
        image = get_image(fi)
        print(fi)
        prediction = get_prediction(fp)
        if threshold:
            atoms = find_peaks_again(prediction)
        else:
            atoms = get_points(fd)
        pngfile = 'a{:04d}.png'.format(i)
        plotfile = os.path.join(videoroot, pngfile)
        makefigure(image, atoms, prediction, plotfile)
        f.write(plotfile + '\n')
        sf.write(pngfile + '\n')

# The images are converted to a movie using the `convert` utility from
# ImageMagick.  Unfortunately, it produces a video that PowerPoint
# cannot play, so it has to be converted again by a video conversion
# utility (I use Wondershare for Mac).
cmd = 'convert -delay 5 -quality 95 @{} {}'.format(os.path.split(shortfilenames)[1], moviename)
with open(scriptname, "wt") as script:
    print("#!/bin/bash", file=script)
    print(cmd, file=script)
# We skip running the actual video generation, as some installations do
# not have imagemagick and ffmpeg installed.

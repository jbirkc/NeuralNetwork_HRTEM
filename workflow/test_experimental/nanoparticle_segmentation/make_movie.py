import re
import glob
from pathlib import Path
from moviepy.editor import *

file_pattern = re.compile(r'.*?(\d+).*?')
def get_order(file):
    match = file_pattern.match(Path(file).name)
    if not match:
        return math.inf
    return int(match.groups()[0])

#folder = '/home/niflheim2/mhlla/hrid/experimental_data/wibang_out/atomicseg/20201201_ETEM_MEMS6/ROI2'
#folders = sorted(glob.glob('{}/DR*'.format(folder)),key=get_order)

folder='/home/niflheim2/mhlla/hrid/experimental_data/wibang_out/4K_segmentation/20210316_ETEM_MEMS8/AuCeO2_defoc9'
folders=glob.glob(folder+'/*')

for dr in folders:
    individual_frames = glob.glob('{}/*.png'.format(dr))
    print('Number of frames:', len(individual_frames))
    
    clip = ImageSequenceClip(individual_frames, fps = 4)
    
    clip.write_videofile('{}/video.avi'.format(dr),codec='png')
    clip.write_gif('{}/video.gif'.format(dr))

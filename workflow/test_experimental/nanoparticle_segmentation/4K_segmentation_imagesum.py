import sys
sys.path.insert(0, '../../../')

import os
import glob

import hyperspy.api as hs
import cv2

import numpy as np
import matplotlib.pyplot as plt
from matplotlib_scalebar.scalebar import ScaleBar

import tensorflow as tf
import tensorflow_addons as tfa
import tensorflow.keras as keras

import cupy as cp

from scipy import ndimage as ndi
from cupyx.scipy import ndimage as cp_ndi
from cupyx.scipy.signal.signaltools import wiener
from cupyx.scipy.fft import fft2, fftshift, ifft

from stm.feature.peaks import find_local_peaks

from codetiming import Timer
from tqdm import tqdm

### Functions ###
def expand_mask(mask, radius=10):
    expand = mask.copy()
    for j in range(radius,mask.shape[1]):
        for k in range(radius,mask.shape[2]):
            window = mask[0,j-radius:j+radius,k-radius:k+radius]
            fraction = np.count_nonzero(window == 1.0)/(window.shape[0]*window.shape[1])
            if fraction > 0.1:
                expand[0,j,k] = 1.0
    return expand

def crop_nanoparticle(img, infer):
    # Make the inference mask binary
    binary = infer[:,:,:,0].copy()
    binary[binary > 0.01] = 1.0
    
    # Expand the inference mask
    crop = expand_mask(binary)
    blur_level=200
    crop = cv2.blur(crop[0,:,:], (blur_level, blur_level))
    crop = crop.reshape((1,)+crop.shape+(1,))
    
    # Crop out nanoparticle
    img_crop = img*crop
    return img_crop, crop

def fft(img, res):
    img_cp = cp.asarray(img)
    img_cp = wiener(img_cp)
    
    f_cp = fft2(img_cp)
    f_cp = fftshift(f_cp)
    
    f = f_cp.get()
    
    cf = f.shape[0]/2,f.shape[1]/2
    f = f[int(cf[0]-res/2):int(cf[0]+res/2),int(cf[1]-res/2):int(cf[1]+res/2)]
    
    f_abs = np.abs(f)
    return (f_abs-np.min(f_abs))/(np.max(f_abs)-np.min(f_abs))

def block_centre(img):
    center = img.shape[0]/2,img.shape[1]/2
    blocked = img.copy()
    blocked[int(center[0]-7):int(center[0]+7),int(center[1]-7):int(center[1]+7)] = 0.0
    return blocked

def cart2pol(x, y):
    rho = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    if phi < 0:
        phi += 2*np.pi
    return rho, phi

def pol2cart(rho, phi):
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return x, y

### Normalisation functions
def local_normalise(image, sigma1, sigma2):
    image = image - ndi.gaussian_filter(image,
                                        (sigma1,sigma1),
                                        mode='reflect')
    image = image / np.sqrt(ndi.gaussian_filter(image**2,
                                                (sigma2,sigma2),
                                                mode='reflect'))   
    return image

def cp_local_normalise(image, sigma1, sigma2):
    image = cp.asarray(image)
    image = image - cp_ndi.gaussian_filter(image,
                                           (sigma1,sigma1),
                                           mode='reflect')
    image = image / cp.sqrt(cp_ndi.gaussian_filter(image**2,
                                                   (sigma2,sigma2),
                                                   mode='reflect'))   
    return image.get()

def tf_local_normalise(image, sigma1, sigma2):
    with tf.device('GPU:0'):
        k = int((2.0*sigma1))
        image = tf.math.subtract(image,
                                     tfa.image.gaussian_filter2d(image,
                                                                 (k,k),
                                                                 (sigma1,sigma1),
                                                                 padding='reflect'))
        k = int((2.0*sigma2))
        image = tf.math.divide(image,
                                  tf.math.sqrt(tfa.image.gaussian_filter2d(tf.math.square(image),
                                                                           (k,k),
                                                                           (sigma2,sigma2),
                                                                           padding='reflect')))
    return image

def gaussian_kernel(size: int, mean: float, std: float):
    """Makes 2D gaussian Kernel for convolution."""
    d = tf.distributions.Normal(mean, std)
    vals = d.prob(tf.range(start = -size, limit = size + 1, dtype = tf.float32))
    gauss_kernel = tf.einsum('i,j->ij',
                                  vals,
                                  vals)
    return gauss_kernel / tf.reduce_sum(gauss_kernel)

def tf_kernel_local_normalise(image, sigma1, sigma2):
    with tf.device('GPU:0'):
        assert sigma1 == sigma2
        sigma = sigma1

        # Make Gaussian Kernel with desired specs.
        gauss_kernel = gaussian_kernel( size = int((4.0*sigma+0.5)),
                                    mean = 0,
                                    std = sigma)
        # Expand dimensions of `gauss_kernel` for `tf.nn.conv2d` signature.
        gauss_kernel = gauss_kernel[:, :, tf.newaxis, tf.newaxis]
        # Convolve.
        gauss_conv = tf.nn.conv2d(image, gauss_kernel, strides=[1, 1, 1, 1], padding="same")
        image_tmp = tf.math.subtract(image,gauss_conv)
        gauss_conv = tf.nn.conv2d(tf.math.square(image_tmp), gauss_kernel, strides=[1, 1, 1, 1], padding="same")
        image_tmp = tf.math.divide(image,tf.math.sqrt(gauss_conv))
    
    return image_tmp

### Contrast scale
def trim_imagescale(img, percent=1):
    sorted_pixels = sorted(img.flatten())
    n = len(sorted_pixels)
    trim = int(n*percent/100)
    return sorted_pixels[trim: n-trim]

def symmetric_peaks(peaks):
    centroid = (sum(peaks[:,0]) / len(peaks), sum(peaks[:,1]) / len(peaks))
    cpeaks = peaks - centroid
    
    # Convert all peaks from cartesian to polar coordinates
    polarpeaks = np.zeros(cpeaks.shape)
    for i, peak in enumerate(cpeaks):
        polarpeaks[i,0], polarpeaks[i,1] = cart2pol(peak[0],peak[1])
    # Sort the peaks by increasing angle
    polarpeaks = np.array(sorted(polarpeaks,key=lambda x: x[1]))

    sym_peaks = [] # this will store symmetric peaks
    for i, polpeak1 in enumerate(polarpeaks):
        # For every peak, loop through all other peaks to find a symmetric peak
        for j, polpeak2 in enumerate(polarpeaks[i+1:]):
            # This if statement matches the length and symmetry (angle+Pi) of every point 
            # to find a symmetric pair
            if (
                polpeak1[0] >= polpeak2[0]-0.01 and polpeak1[0] <= polpeak2[0]+0.01 and 
                polpeak1[1]+np.pi >= polpeak2[1]-0.01 and polpeak1[1]+np.pi <= polpeak2[1]+0.01
            ):
                cartp1_x, cartp1_y = pol2cart(polpeak1[0],polpeak1[1])
                cartp2_x, cartp2_y = pol2cart(polpeak2[0],polpeak2[1])
                
                sym_peaks.append( (cartp1_x+centroid[0], cartp1_y+centroid[1],
                                  cartp2_x+centroid[0], cartp2_y+centroid[1]) )
    return sym_peaks

def summed_peaks(img, peaks):
    sum_peaks = 0
    for p in peaks:
        sum_peaks += img[int(p[0]),int(p[1])]
    
    return sum_peaks
    
def brightness_ratio(img, peaks):
    if len(peaks)>1:
        br = []
        for p in peaks:
            br.append( img[int(p[0]),int(p[1])] + img[int(p[2]),int(p[3])] )

        return min(br)/max(br)
    else:
        return 0

###########

folder='../../experimental_data/wibang/20210316_ETEM_MEMS8/AuCeO2_defoc9'
files=glob.glob(r'{}/Hour_00/Minute_*/Second_*/*.dm4'.format(folder))
outfolder='4K_segmentation_output_imagesum'
if not os.path.exists(outfolder):
    os.mkdir(outfolder)
print('Files contain {} frames.'.format(len(files)))

### Load trained neural network
n = 'MSDnet'
network = 'Au_fcc_mixed'
#Trained Neural net folder
nnf = '../../{}_precomputed_trained_data/{}'.format(n,network) 
nnf = glob.glob(os.path.join(nnf,'model-*'))[-1] # latest model
print("Using {} CNN model in".format(n), network)
#nnf = '../../MSDnet/MSDnet60_07122021/'
#nnf = glob.glob(os.path.join(nnf,'model-*'))[-1] # latest model
mod = keras.models.load_model(nnf)

fullres = 4096
winres = 512
ftres = 160
nw = int(fullres/winres)

t = Timer(name='4K Segmentation of {} frames.'.format(len(files)),
        text='{name}: Elapsed time: {:.4f} seconds')
    
imgsum = np.zeros((fullres,fullres))
imgsum_br = np.zeros((fullres,fullres))
cnt_br = 0
imgsum_sum = np.zeros((fullres,fullres))
cnt_sum = 0
t.start()
for fidx, f in enumerate(files):
    #toggle_br = False
    #toggle_sum = False
    
    ### Load data
    d = hs.load(f)
    
    x_cal, y_cal=d.axes_manager.signal_axes[0].scale, \
            d.axes_manager.signal_axes[1].scale
    if x_cal > 0.008 or x_cal < 0.007:
        continue
    
    assert x_cal == y_cal
    sigma = 12.0/(x_cal*10)
    
    # normalise with cupy ndimage
    tt = Timer(name="cupy normalise", text="{name}: {:.4f} seconds")
    tt.start()
    image_norm = cp_local_normalise(d.data, sigma, sigma)
    tt.stop()
    
    tt = Timer(name="image scale", text="{name}: {:.4f} seconds")
    tt.start()
    trimmed_pixels = trim_imagescale(image_norm)            # Trim away the upper and lower 1% (default)
    scale = min(trimmed_pixels), max(trimmed_pixels)        # Save the values to scale
    tt.stop()
    print(scale)

    tt = Timer(name="window segmentation", text="{name}: {:.4f} seconds")
    tt.start()
    for x in tqdm(range(nw),desc='Column',position=0):
        for y in tqdm(range(nw),desc='Row',position=1,leave=False):
            img = image_norm[x*winres:(x+1)*winres,
                             y*winres:(y+1)*winres]
            img.shape = (1,) + img.shape + (1,)
            infer = mod.predict(img)

            # Cut out nanoparticle
            imgcrop, crop = crop_nanoparticle(img, infer)
            # Get FT
            imgcrop_ft = fft(imgcrop[0,:,:,0],ftres)
            # Locate peaks
            imgcrop_ftnorm = block_centre(imgcrop_ft)
            peaks = find_local_peaks(imgcrop_ftnorm,
                                     min_distance=30,
                                     threshold=0.3,
                                     local_threshold=0,
                                     exclude_adjacent=True)

            sym_peaks = symmetric_peaks(peaks)
            sum_peaks = summed_peaks(imgcrop_ftnorm, sym_peaks)
            br_peaks = brightness_ratio(imgcrop_ftnorm, sym_peaks)
            
            imgsum[x*winres:(x+1)*winres,
                   y*winres:(y+1)*winres] += img[0, :, :, 0]
            if fidx > 0:
                imgsum[x*winres:(x+1)*winres,
                       y*winres:(y+1)*winres] /= 2
            if br_peaks > 0.6:
                #toggle_br = True
                imgsum_br[x*winres:(x+1)*winres,
                          y*winres:(y+1)*winres] += img[0, :, :, 0]
                if cnt_br > 0:
                    imgsum_br[x*winres:(x+1)*winres,
                              y*winres:(y+1)*winres] /= 2
                    cnt_br += 1
            if sum_peaks > 1.0:
                #toggle_sum = True
                imgsum_sum[x*winres:(x+1)*winres,
                           y*winres:(y+1)*winres] += img[0, :, :, 0]
                if cnt_sum > 0:
                    imgsum_sum[x*winres:(x+1)*winres,
                               y*winres:(y+1)*winres] /= 2
                    cnt_sum += 1
    tt.stop()
    
    # Normalise frames added 
    #if fidx > 0:
    #    imgsum /= 2
    #if cnt_br > 1 and toggle_br:
    #    imgsum_br /= 2
    #if cnt_sum > 1 and toggle_sum:
    #    imgsum_sum /= 2

    fig, ax = plt.subplots(1,3,figsize=(24,12))

    pos = ax[0].imshow(imgsum.T, origin='lower', cmap='gray', vmin=scale[0], vmax=scale[1])
    fig.colorbar(pos, ax=ax[0], anchor=(0, 0.3), shrink=0.2)
    ax[0].add_artist(ScaleBar(x_cal,units='nm',length_fraction=0.2,color='k',frameon=True,location='lower right'))
    
    pos = ax[1].imshow(imgsum_br.T, origin='lower', cmap='gray', vmin=scale[0], vmax=scale[1])
    fig.colorbar(pos, ax=ax[1], anchor=(0, 0.3), shrink=0.2)
    ax[1].add_artist(ScaleBar(x_cal,units='nm',length_fraction=0.2,color='k',frameon=True,location='lower right'))
    
    pos = ax[2].imshow(imgsum_sum.T, origin='lower', cmap='gray', vmin=scale[0], vmax=scale[1])
    fig.colorbar(pos, ax=ax[2], anchor=(0, 0.3), shrink=0.2)
    ax[2].add_artist(ScaleBar(x_cal,units='nm',length_fraction=0.2,color='k',frameon=True,location='lower right'))

    plt.tight_layout()
    plt.savefig('{}/frame_{}.png'.format(outfolder,str(fidx).zfill(3)))
    plt.close()

t.stop()

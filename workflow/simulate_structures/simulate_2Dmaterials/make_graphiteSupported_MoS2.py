from maker import SampleMaker, main
from flake import Flake
from ase import Atoms
from ase.build import mx2
from scipy.cluster.hierarchy import fcluster, linkage
import numpy as np


class GraphiteSupportedMoS2Maker(SampleMaker):
    """Makes samples of MoS2 on a graphene support.

    The graphene is ignored in the sites and classes output.  Columns
    containing at least one Mo are class 0, those containing S are 
    class 1.  No columns should contain both Mo and S, but if present they
    will be class 0.
    """
    columndistance = 0.5   # Max horizontal distance between atoms in column, in Å.
    numclasses = 2
    species = (42, 16)
    save_properties = {
        'vacancies': 'n_vac',
        'suppport_vacancies': 'n_supvac', 
        'holes': 'n_holes', 
        'tilt': 'tilt',
        'graphitelayers': 'graphitelayers',
        }

    def __init__(self, size, distort, seed, graphitelayers):
        super().__init__(distort)
        self.rng = np.random.default_rng(seed)
        prototype = mx2(formula='MoS2')
        self.size = size
        self.flake = Flake(prototype, size, self.rng)
        a, c = 2.46, 6.7  # Lattice parameters of graphite
        cell = [[a, 0, 0], [-0.5*a, np.sqrt(3)/2 * a, 0], [0, 0, c]]
        positions = [[0,0,0], [0, a/np.sqrt(3), 0], [0.5*a, 0.5*a/np.sqrt(3), c/2], [-a/2, np.sqrt(3)/2*a, c/2]]
        prototype = Atoms(symbols='CCCC', positions=positions, cell=cell, pbc=[True,True,False])
        prototype *= (1,1,(graphitelayers+1)//2)
        self.supportflake = Flake(prototype, size, self.rng)
        self.maxgraphitelayers = graphitelayers
        self.support_c = c
            
    def make_data(self, _unused):
        allatoms = self.make_atoms()
        atoms = allatoms[allatoms.numbers != 6]   # Discard C atoms
        
        # Create positions of the columns.
        positions = atoms.get_positions()[:,:2]
        z = atoms.get_atomic_numbers()
        clusters = fcluster(linkage(positions), self.columndistance, criterion='distance')
        unique = np.unique(clusters)
        # Now cluster is the column number each atom belongs to, and unique is a list of all columns
        sites = np.zeros((len(unique), 2))    # Positions of the colums
        classes = -np.ones(len(unique), int)  # classes of the columns.  Initialize to -1
        for i, u in enumerate(unique):
            sites[i] = np.mean(positions[clusters==u], axis=0)
            atnos = z[clusters==u]
            for j, c in enumerate(self.species):
                if c in atnos:
                    classes[i] = j
        return allatoms, positions, sites, classes

    def make_atoms(self):
        self.supportflake.make(scale=1.1)
        self.graphitelayers = int(self.rng.integers(1, self.maxgraphitelayers, endpoint=True)) # Make a radnom number of layers.
        # Remove excess layer
        z_max = self.support_c * self.graphitelayers / 2 - self.support_c/4
        self.supportflake.set_thickness(z_max)
        self.n_supvac = self.supportflake.vacancies(0.05, 'C')
        self.flake.make(scale=0.9)
        # Some samples should have regular relative orientation, other should be random
        self.flake.rotate(self.rng.choice([0, 15, 30, self.rng.uniform(0.0, 90.0)]))
        self.n_vac = self.flake.vacancies(0.05, 'S')
        self.n_holes = self.flake.holes(0.05)
        self.flake.stack(self.supportflake, (3.3, 7.0))
        if self.distort:
            # Some samples should have larger displacements of atoms than others
            # Pull typical displacement from lognormal distribution with mean of 0.1 Å
            displ = np.clip(self.rng.lognormal(np.log(self.distort), np.exp(-1)), 0.0, 0.5)
            self.flake.perturb_positions(displ)
        else:
            self.flake.perturb_positions(0.01)
        self.flake.rotate()
        self.tilt = self.flake.tilt(1) 
        return self.flake.get_atoms()

    def remove_support(self, atoms):
        "Remove carbon support"
        top_of_cell = atoms.cell[2, 2]
        
        z = atoms[atoms.numbers != 6].positions[:, 2]
        distance_to_cell_top = abs(z.max() - top_of_cell)
        
        newpositions = atoms.get_positions()
        newpositions[:, 2] += distance_to_cell_top - 1.5
        atoms.set_positions(newpositions)
        print()
        print("atoms max(z)")
        print("Before:", end="\t")
        print(z.max())
        print("After:", end="\t")
        print(atoms.positions[:,2].max())
        print()
        print("end")
        print("-----------")
        
        return atoms[atoms.numbers != 6]

def makeMoS2(first_number, last_number, dir_name, size, distort, seed, graphitelayers):
    print("Generating images {} - {}".format(first_number, last_number))
    print("Output folder:", dir_name)
    print("System size: {:.1f} Å x {:.1f} Å".format(size, size))
    if distort:
        print("Atomic distortion: {:.3f} Å".format(distort))
    print("Seed:", seed)
    maker = GraphiteSupportedMoS2Maker((size, size), distort, seed, graphitelayers)
    maker.run(first_number, last_number, dir_name)


if __name__ == "__main__":
    main(makeMoS2, __file__, graphite=True)
    


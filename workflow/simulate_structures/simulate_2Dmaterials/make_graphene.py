from maker import SampleMakerTrue2D, main
from flake import Flake
from ase import Atoms
import numpy as np

class GrapheneMaker(SampleMakerTrue2D):
    save_properties = {'vacancies': 'n_vac', 'tilt': 'tilt'}

    def __init__(self, size, distort, seed):
        super().__init__(distort)
        a = 2.46
        cell = [[a, 0, 0], [-0.5*a, np.sqrt(3)/2 * a, 0], [0, 0, 2]]
        positions = [[0,0,1], [0, a/np.sqrt(3), 1]]
        prototype = Atoms(symbols='CC', positions=positions, cell=cell, pbc=[True,True,False])
        self.size = size
        self.flake = Flake(prototype, size, seed)

    def make_atoms(self):
        self.flake.make()
        self.flake.rotate()
        self.n_vac = self.flake.vacancies(0.1)
        self.flake.perturb_positions(0.1)
        self.tilt = self.flake.tilt(10)
        return self.flake.get_atoms()
    
def makegraphene(first_number, last_number, dir_name, size, distort, seed):
    print("Generating images {} - {}".format(first_number, last_number))
    print("Output folder:", dir_name)
    print("System size: {:.1f} Å x {:.1f} Å".format(size, size))
    if distort:
        print("Atomic distortion: {:.3f} Å".format(distort))
    print("Seed:", seed)
    maker = GrapheneMaker((size, size), distort, seed)
    maker.run(first_number, last_number, dir_name)


if __name__ == "__main__":
    main(makegraphene, __file__)
    

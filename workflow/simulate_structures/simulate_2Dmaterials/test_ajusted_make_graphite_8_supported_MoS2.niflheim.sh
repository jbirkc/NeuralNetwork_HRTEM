#!/bin/bash
#SBATCH --job-name=test-MoS2-sup-8-layers
#SBATCH --mail-type=fail,END
#SBATCH --partition=xeon16
#SBATCH --output=slurm-%x-%j.out
#SBATCH --time=1:00:00
#SBATCH -N 1
#SBATCH -n 1
#
# Necessary modules
source $HOME/bachelor/bachelor-venv/bin/activate
#
python make_graphiteSupported_MoS2.py test_adjusted_MoS2_supported_layers_8 1000 --graphite 8 --train 
python make_graphiteSupported_MoS2.py test_adjusted_MoS2_supported_layers_8 1000 --graphite 8 --test

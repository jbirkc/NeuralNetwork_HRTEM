import time
import os
import stat
import subprocess

def make_reproducible(filename, scriptname, args, positional):
    """Write a file so a script can be called again reproducing the results.

    filename:
        Path to the shell script produced.
    scriptname:
        Name of Python script, typically from parser.proc.
    args:
        Dictionary of arguments
    positional:
        Tuple of the argument names of the positional arguments, in order.

    Note: args should be a dictionary obtained from the Namespace object
    returned by argparse.ArgumentParser.parse_args() i.e. by calling
    ``vars()`` on it.  Any argument that leads to non-reproducible
    results should be replaced by a reproducible value, this could be a
    seed for the random number generator or the number of cpus.

    Any argument with a boolean value is assumed to be a flag, and is
    added to the command line if the value is True.
    """

    try:
        githash = subprocess.check_output(['git', 'rev-parse', 'HEAD'], universal_newlines=True).strip()
    except (subprocess.CalledProcessError, FileNotFoundError):
        githash = 'Unknown'
    with open(filename, "wt") as repro:
        repro.write("#!/bin/bash\n\n")
        repro.write("# Date: {}\n".format(time.asctime()))
        repro.write("# Git hash: {}\n".format(githash))
        repro.write("\n")
        cmd = "python {}".format(scriptname)
        argdict = args.copy() 
        # First run through the positional arguments
        for a in positional:
            cmd += " '{}'".format(argdict[a])
            del argdict[a]
        for a, v in argdict.items():
            if v is None:
                # Skip it so the default is kept, as None is rarely an acceptable value
                continue
            if isinstance(v, bool):
                if v:
                    cmd += ' --{}'.format(a)
            else:
                cmd += " --{} '{}'".format(a, v)
        repro.write(cmd + '\n')

    # Make the script executable
    st = os.stat(filename)
    os.chmod(filename, st.st_mode | stat.S_IEXEC)


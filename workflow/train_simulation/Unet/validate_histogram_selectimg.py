"""validate_histogram.py - create a histogram of the network performance

Usage: python validate_histogram.py testdatafolder modelfolder [stepsubfolder]

This is a special version of the script allowing for data containing more images
than used by the neural network - this is used for data that is also used for a
traditional exit wave reconstruction.  As this complicates the script for a 
rarely-used feature, a separate version is made.
"""

import sys
sys.path.insert(0, '../')

#import matplotlib
#matplotlib.use('AGG')

# from train_imageepochs import datagenerator
from train_imageepochs import cp_local_normalise
import argparse
import os
import json
import numpy as np
from tensorflow import keras
import tensorflow as tf

def my_rmse(label, pred):
    assert (len(pred.shape) == 3 and pred.shape[2] == 2) or len(pred.shape) == 2  
    assert label.shape == pred.shape
    mse = np.mean((pred - label)**2)
    return np.sqrt(mse)

def my_rmss(label):
    mss = np.mean(label**2)
    return np.sqrt(mss)

def my_mae(label, pred):
    assert (len(pred.shape) == 3 and pred.shape[2] == 2) or len(pred.shape) == 2  
    assert label.shape == pred.shape
    return np.mean(np.abs(pred - label))

class datagenerator(tf.keras.utils.Sequence):
    def __init__(self, 
            batch_size, 
            img_size,
            data_paths,
            params_paths,
            input_channels,
            output_channels,
            normalisation_distance,
            augment=True,
            selectimages=None):
         
         self.batch_size = batch_size
         self.img_size = img_size
         self.data_paths = data_paths
         self.params_paths = params_paths
         self.input_channels = input_channels
         self.output_channels = output_channels
         self.normalisation_distance = normalisation_distance
         self.augment = augment
         self.selectimages = selectimages
         
         assert len(self.data_paths) == len(self.params_paths)
         self.n = len(self.data_paths)
         self.indexes = np.arange(self.n)

    def on_epoch_end(self):
        'updates indexes after each epoch'
        #np.random.shuffle(self.indexes)
    
    def __getitem__(self, index):
        i = self.indexes[index * self.batch_size]
        batch_data_paths = self.data_paths[i : i + self.batch_size]
        batch_params_paths = self.params_paths[i : i + self.batch_size]

        return self.__dataloader(self.img_size,
                batch_data_paths, batch_params_paths,
                self.input_channels, self.output_channels,
                self.normalisation_distance)
    
    def __len__(self):
        return self.n // self.batch_size

    #################### data loader ########################
    def __dataloader(self, 
            img_size,
            data_paths,
            param_paths,
            input_channels,
            output_channels,
            normalisation_distance):
        x = np.zeros((len(data_paths), img_size[0], img_size[1], input_channels))
        y = np.zeros((len(data_paths), img_size[0], img_size[1], output_channels))
        for j, (data_path, param_path) in enumerate(zip(data_paths, param_paths)):
            with open(param_path) as json_file:
                p = json.load(json_file)
            sigma = normalisation_distance/p['sampling']
            data = np.load(data_path)

            if self.selectimages:
                assert len(self.selectimages) == input_channels, f'{len(self.selectimages)} {input_channels}'
                img_tmp = data['image'][:,:img_size[0],:img_size[1],self.selectimages]
            else:
                img_tmp = data['image'][:,:img_size[0],:img_size[1],:]
            lbl_tmp = data['label'][:,:img_size[0],:img_size[1],:]
            img_tmp = cp_local_normalise(img_tmp[0,:,:,:], sigma, sigma)
            img_tmp.shape = (1,) + img_tmp.shape 
            if self.augment:
                img_tmp = random_brightness(img_tmp, -0.1, 0.1)
                img_tmp = random_contrast(img_tmp, 0.9, 1.1)
                img_tmp = random_gamma(img_tmp, 0.9, 1.1)
                img_tmp, lbl_tmp = random_flip(img_tmp, lbl_tmp)
            
            x[j] = img_tmp
            y[j] = lbl_tmp

        return x, y


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "data",
        help="The pathname of the folder where the test data is placed."
        )
    parser.add_argument(
        "model",
        help="The pathname of the folder where the trained model is placed."
        )
    parser.add_argument(
        "modelstep",
        help="The subfolder with the model - default: 'model-0'.",
        nargs='?',
        default="model-0"
        )
    parser.add_argument(
        "-o", "--output",
        help="Name of output file",
        default=None
        )
    parser.add_argument(
        "--selectimages",
        help="Select these images if more are included in files.",
        default=None
        )
    args = parser.parse_args()

    btch = 10
    with open(os.path.join(args.data, 'parameters.json'), "rt") as pfile:
        dataparameters = json.load(pfile)

    imgdim = tuple(dataparameters['image_size'])

    try:
        chan_in = dataparameters['multifocus'][0]
    except (KeyError, TypeError):
        chan_in = 1
    chan_out = dataparameters.get('num_classes', 1)

    if args.selectimages:
        selectimages = eval(args.selectimages)
        assert len(selectimages) < chan_in
        chan_in = len(selectimages)
        print(f"Selecting images {selectimages} from input file (changing channels to {chan_in}).")
    else:
        selectimages = None

    if args.output:
        print(f'Writing output to file named {args.output}', flush=True)
        
    #########################################################
    ################ load validation data ###################
    ### the input folders have been identified, including the
    ### validation dataset folder, here we load that in.
    ### keras will use this to validate the network after
    ### each training epoch
    #########################################################
    vinput_data_dir = args.data + '/images_labels'
    vdata_paths = sorted(
        [
            os.path.join(vinput_data_dir, fname)
            for fname in os.listdir(vinput_data_dir)
            if fname.endswith(".npz")
            ])
    vinput_params_dir = args.data + '/tem_params'
    vparams_paths = sorted(
        [
            os.path.join(vinput_params_dir, fname)
            for fname in os.listdir(vinput_params_dir)
            ])
    if len(vparams_paths) == 1:
        # Hack for historical data
        vparams_paths = [vparams_paths[0]] * len(vdata_paths)
    print('validation data:', len(vdata_paths), ' samples')
    val_gen = datagenerator(
            btch, imgdim, vdata_paths, vparams_paths, chan_in, chan_out,
            dataparameters['normalizedistance'], augment=False,
            selectimages=selectimages
    )

    print("\n\nLoading model", flush=True)
    modelname = os.path.join(args.model, args.modelstep)
    model = keras.models.load_model(modelname)
    
    print("\n\nMaking predictions", flush=True)
    predictions = model.predict(val_gen, verbose=1)

    print("Extracting labels ...", flush=True)
    labels = np.concatenate([x[1] for x in val_gen])
    print(len(labels))
    print(labels.shape, labels.dtype)
    print("... done.  Shape:", predictions.shape, predictions.dtype, labels.shape, labels.dtype)

    print(my_rmse(labels[0], predictions[0]), my_mae(labels[0], predictions[0]))
    print(my_rmse(labels[1], predictions[1]), my_mae(labels[1], predictions[1]))
    print(my_rmse(labels[0], predictions[1]), my_mae(labels[0], predictions[1]))

    print("Calculating", flush=True)
    rmse = np.array( [my_rmse(labels[i], predictions[i]) for i in range(len(labels))] )
    rmse_re = np.array( [my_rmse(labels[i,:,:,0], predictions[i,:,:,0]) for i in range(len(labels))] )
    rmse_im = np.array( [my_rmse(labels[i,:,:,1], predictions[i,:,:,1]) for i in range(len(labels))] )

    rel_rmse = np.array( [my_rmse(labels[i], predictions[i])  / my_rmss(labels[i])
                              for i in range(len(labels))] )

    mae = np.array( [my_mae(labels[i], predictions[i]) for i in range(len(labels))] )
    mae_re = np.array( [my_mae(labels[i,:,:,0], predictions[i,:,:,0]) for i in range(len(labels))] )
    mae_im = np.array( [my_mae(labels[i,:,:,1], predictions[i,:,:,1]) for i in range(len(labels))] )

    
    scram = np.arange(len(labels))
    np.random.shuffle(scram)
    scram_rmse = np.array( [my_rmse(labels[i], predictions[scram[i]]) for i in range(len(labels))] )
    scram_rmse_re = np.array( [my_rmse(labels[i,:,:,0], predictions[scram[i],:,:,0]) for i in range(len(labels))] )
    scram_rmse_im = np.array( [my_rmse(labels[i,:,:,1], predictions[scram[i],:,:,1]) for i in range(len(labels))] )

    scram_mae = np.array( [my_mae(labels[i], predictions[scram[i]]) for i in range(len(labels))] )
    scram_mae_re = np.array( [my_mae(labels[i,:,:,0], predictions[scram[i],:,:,0]) for i in range(len(labels))] )
    scram_mae_im = np.array( [my_mae(labels[i,:,:,1], predictions[scram[i],:,:,1]) for i in range(len(labels))] )

    if args.output is None:
        output = os.path.join(args.model, f"histogram-epoch-{args.modelstep}.npz")
    else:
        output = os.path.join(args.model, args.output)
        
    np.savez_compressed(output,
                        rmse=rmse, mae=mae, rel_rmse=rel_rmse,
                        rmse_re=rmse_re, rmse_im=rmse_im,
                        mae_re=mae_re, mae_im=mae_im,
                        scram_rmse=scram_rmse, scram_mae=scram_mae,
                        scram_rmse_re=scram_rmse_re, scram_rmse_im=scram_rmse_im,
                        scram_mae_re=scram_mae_re, scram_mae_im=scram_mae_im
                        )
    print(f"Result saved to {output}")

    
if __name__ == "__main__":
    main()
    

import sys
import os
import glob
import json
import platform
import contextlib
import time
import argparse
import shutil

import numpy as np
import cupy as cp
#os.environ['TF_CPP_MIN_LOG_LEVEL'] = '1'  # or any {'0', '1', '2'}
#os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'
import tensorflow as tf
#import tensorflow_addons as tfa
from tensorflow.keras import mixed_precision
from tensorflow.keras import layers
from tensorflow.keras import regularizers
from tensorflow.keras import activations
from tensorflow.keras import initializers
from tensorflow.keras.optimizers import Adam, RMSprop

from temnn.knet.ReflectionPadding2D import ReflectionPadding2D
from temnn.reproducible import make_reproducible

from skimage.filters import gaussian
from scipy.ndimage.filters import gaussian_filter
from cupyx.scipy import ndimage as cp_ndi

from datetime import datetime
from tqdm import tqdm

###################################################
#### Default parameters for the Neural Network ####
###################################################

netparams_segment = dict(
    levels=4,
    nchannels=64,
    kernel_size=9
)
netparams_exitwave = dict(
    levels=4,
    nchannels=24,
    kernel_size=5
)

#########################################################
#### tensorflow mixed precision for memory efficiency ###
#########################################################
policy = mixed_precision.Policy('float32')
mixed_precision.set_global_policy(policy)

#########################################################
########## construct net architecture ###################
#########################################################
def get_model(img_dim,
              num_channels,
              output_layer,
              regul,
              levels,
              nchannels,
              kernel_size):
    inputs = keras.Input(shape=img_dim)
    x = inputs
    
    skip_list = []
    # Downsampling blocks
    channel_list = [nchannels * 2**i for i in range(levels)]
    for channels in channel_list[:-1]:
        # Entry block
        x = conv_block(x, channels, kernel_size, regul)
        previous_block_activation = x # set aside residual
        # Residual block
        x = conv_block(x, channels, kernel_size, regul)
        x = conv_block(x, channels, kernel_size, regul)
        x = conv_block(x, channels, kernel_size, regul)
        x = layers.concatenate([x, previous_block_activation])
        # Exit block
        x = conv_block(x, channels, kernel_size, regul)
        skip_list.append((channels, x))
        # Pool
        x = layers.MaxPooling2D(pool_size=2, padding='same')(x)
    
    # Bridge
    # Entry block
    brchan = channel_list[-1]
    x = conv_block(x, brchan, kernel_size, regul)
    previous_block_activation = x # set aside residual
    # Residual block
    x = conv_block(x, brchan, kernel_size, regul)
    x = conv_block(x, brchan, kernel_size, regul)
    x = conv_block(x, brchan, kernel_size, regul)
    x = layers.concatenate([x, previous_block_activation])
    # Exit block
    x = conv_block(x, brchan, kernel_size, regul)
    
    # Upsampling blocks
    for channels, skip in skip_list[::-1]:
        # Upsample
        x = layers.UpSampling2D(2, interpolation='bilinear')(x)
        x = conv_block(x, channels, regul=regul, kernel_size=1)
        x = layers.concatenate([x,skip])
        # Entry block
        x = conv_block(x, channels, kernel_size, regul)
        previous_block_activation = x # set aside residual
        # Residual block
        x = conv_block(x, channels, kernel_size, regul)
        x = conv_block(x, channels, kernel_size, regul)
        x = conv_block(x, channels, kernel_size, regul)
        x = layers.concatenate([x, previous_block_activation])
        # Exit block
        x = conv_block(x, channels, kernel_size, regul)

    if output_layer == 'segment':
        # Output layer is classification
        if num_channels > 1:
            final_act = "softmax"
        else:
            final_act = "sigmoid"
    elif output_layer == 'regress':
        # Output layer is regression
        final_act = None
    # add a per-pixel output layer (regression/classification)
    if regul:
        biasregul = regularizers.l2(regul)
        kernelregul = regularizers.l2(regul)
    else:
        biasregul = kernelregul = None
    outputs = layers.Conv2D(
                filters=num_channels,
                kernel_size=1,
                activation=final_act,
                padding="same",
                kernel_initializer='RandomNormal',
                bias_initializer=initializers.Constant(0.1),
                kernel_regularizer=kernelregul,
                bias_regularizer=biasregul
                )(x)
    # define the model
    model = keras.Model(inputs, outputs)
    return model

def conv_block(x, filters, kernel_size, regul):
    if regul:
        biasregul = regularizers.l2(regul)
        kernelregul = regularizers.l2(regul)
    else:
        biasregul = kernelregul = None
    #x = layers.ZeroPadding2D(padding=((1,1)))(x)
    #x = ReflectionPadding2D(padding=((kernel_size-1),
    #                                 (kernel_size-1)))(x)
    x = layers.Conv2D(
            filters=filters,
            kernel_size=kernel_size,
            padding="same",  # 'valid' for reflectionpadding
            #activation=lambda x: activations.relu(x, alpha=0.1),
            kernel_initializer='RandomNormal',
            bias_initializer=initializers.Constant(0.1),
            kernel_regularizer=kernelregul,
            bias_regularizer=biasregul
            )(x)
    x = layers.PReLU(
            shared_axes=[1,2],
            alpha_initializer=initializers.Constant(0.01),
            )(x)
    x = layers.BatchNormalization()(x)
    #x = layers.Activation("relu")(x)
    #x = layers.Dropout(rate=0.5)(x)
    
    return x

#########################################################
################## data generator #######################
#########################################################
class datagenerator(tf.keras.utils.Sequence):
    def __init__(self, 
            batch_size, 
            img_size,
            data_paths,
            params_paths,
            input_channels,
            output_channels,
            normalisation_distance,
            augment=True,
            augment_factor=1):
         
         self.batch_size = batch_size
         self.img_size = img_size
         self.data_paths = data_paths
         self.params_paths = params_paths
         self.input_channels = input_channels
         self.output_channels = output_channels
         self.normalisation_distance = normalisation_distance
         self.augment = augment
         if self.augment:
             self.augment_factor = augment_factor
         else:
             self.augment_factor = 1
         assert len(self.data_paths) == len(self.params_paths)
         self.n = len(self.data_paths)
         self.indexes = np.arange(self.n)

    def on_epoch_end(self):
        'updates indexes after each epoch'
        np.random.shuffle(self.indexes)
    
    def __getitem__(self, index):
        i = self.indexes[index * self.batch_size]
        batch_data_paths = self.data_paths[i : i + self.batch_size]
        batch_params_paths = self.params_paths[i : i + self.batch_size]

        return self.__dataloader(self.img_size,
                batch_data_paths, batch_params_paths,
                self.input_channels, self.output_channels,
                self.normalisation_distance)
    
    def __len__(self):
        return self.n // self.batch_size

    #################### data loader ########################
    def __dataloader(self, 
            img_size,
            data_paths,
            param_paths,
            input_channels,
            output_channels,
            normalisation_distance):
        x = np.zeros((len(data_paths)*self.augment_factor, img_size[0], img_size[1], input_channels))
        y = np.zeros((len(data_paths)*self.augment_factor, img_size[0], img_size[1], output_channels))
        for j, (data_path, param_path) in enumerate(zip(data_paths, param_paths)):
            with open(param_path) as json_file:
                p = json.load(json_file)
            sigma = normalisation_distance/p['sampling']
            data = np.load(data_path)
            
            img_tmp = data['image'][:,:img_size[0],:img_size[1],:]
            lbl_tmp = data['label'][:,:img_size[0],:img_size[1],:]
            img_tmp = cp_local_normalise(img_tmp[0,:,:,:], sigma, sigma)
            img_tmp.shape = (1,) + img_tmp.shape
            if self.augment:
                if self.augment_factor > 1:
                    x[j] = img_tmp
                    y[j] = lbl_tmp
                   
                img_tmp = random_brightness(img_tmp, -0.1, 0.1)
                img_tmp = random_contrast(img_tmp, 0.9, 1.1)
                img_tmp = random_gamma(img_tmp, 0.9, 1.1)
                img_tmp, lbl_tmp = random_flip(img_tmp, lbl_tmp)
                
                if self.augment_factor > 1:
                    x[-(j+1)] = img_tmp
                    y[-(j+1)] = lbl_tmp
                else:
                    x[j] = img_tmp
                    y[j] = lbl_tmp
            else:
                x[j] = img_tmp
                y[j] = lbl_tmp
        
        p = np.random.permutation(len(data_paths)*self.augment_factor)
        x, y = x[p,:,:,:], y[p,:,:,:]
        
        return x, y

#########################################################
################## image processing #####################
#########################################################
def cp_local_normalise(image, sigma1, sigma2):
    image = cp.asarray(image)
    channels = image.shape[-1]
    if channels > 1:
        B=cp.zeros_like(image[:,:,0])
        S=cp.zeros_like(image[:,:,0])
        for i in range(channels):
            B += cp_ndi.gaussian_filter(image[:,:,i],
                                        (sigma1,sigma1),
                                        mode='reflect')
        for i in range(channels):
            image[:,:,i] = image[:,:,i] - B/channels
        
        for i in range(channels):
            S += cp.sqrt(cp_ndi.gaussian_filter(image[:,:,i]**2,
                                                (sigma2,sigma2),
                                                mode='reflect'))
        for i in range(channels):
            image[:,:,i] = image[:,:,i] / (S/channels)
    else:
        image[:,:,0] = image[:,:,0] - cp_ndi.gaussian_filter(image[:,:,0],
                                                             (sigma1,sigma1),
                                                             mode='reflect')
        image[:,:,0] = image[:,:,0] / cp.sqrt(cp_ndi.gaussian_filter(image[:,:,0]**2,
                                                                     (sigma2,sigma2),
                                                                      mode='reflect'))   
    return image.get()

def random_flip(images, labels, rnd=np.random.rand):
    for i in range(len(images)):
        if rnd() < .5:
            images[i,:,:,:] = np.fliplr(images[i,:,:,:])
            labels[i,:,:,:] = np.fliplr(labels[i,:,:,:])
            
        if rnd() < .5:
            images[i,:,:,:] = np.flipud(images[i,:,:,:])
            labels[i,:,:,:] = np.flipud(labels[i,:,:,:])
    return images,labels

def random_brightness(images, low, high, rnd=np.random.uniform):
    for i in range(len(images)):
        images[i,:,:,0]=images[i,:,:,0]+rnd(low,high)
    return images

def random_contrast(images, low, high, rnd=np.random.uniform):
    for i in range(len(images)):
        mean=np.mean(images[i,:,:,0])
        images[i,:,:,0]=(images[i,:,:,0]-mean)*rnd(low,high)+mean
    return images
    
def random_gamma(images, low, high, rnd=np.random.uniform):
    for i in range(len(images)):
        min=np.min(images[i,:,:,0])
        images[i,:,:,0]=(images[i,:,:,0]-min)*rnd(low,high)+min
    return images

#########################################################
################## START OF MAIN CODE ###################
#########################################################
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    positionalparams = ('epochs', 'datf', 'vdatf', 'outf')
    parser.add_argument("epochs", default=10, type=int,
            help="Number of training epochs.")
    parser.add_argument("datf",
            help="The path and name of the folder where the training data is placed.")
    parser.add_argument("vdatf",
            help="The path and name of the folder where the validation data is placed..")
    parser.add_argument("outf",
            help="The name of the folder where the output is placed.")
    parser.add_argument("--restart", default=None, type=int, help="Restart at epoch N.")
    parser.add_argument("--regularize", default=None, type=float,
                        help="Amount of L2 regularization (default: disabled).")
    parser.add_argument("--adam", default=None, type=str,
                        help="Parameters for Adam optimizers, as a string containing a dict.")
    parser.add_argument("--rmsprop", default=None, type=float,
                        help="Use RMSprop with this learning rate (instead of Adam).")
    parser.add_argument("--augment_factor", default=1, type=int,
                        help="Expand training data by this factor to store original and augmented data per epoch.")
    args = parser.parse_args()

    ### Create output folder ###
    if not args.outf.endswith('/'):
        outf = args.outf + '/'
    else:
        outf = args.outf
    if not os.path.exists(outf):
        print('Creating ', outf)
        os.makedirs(outf)
    ### Keep a copy of this script for reference ###
    shutil.copy2(__file__, outf)
    argdict = vars(args).copy()
    make_reproducible(os.path.join(outf, 'reproduce_train.sh'),
                      parser.prog,
                      argdict,
                      positionalparams)
    
    #########################################################
    ################ determine number of gpus ###############
    ### if there are gpus availables they will be located and
    ### utilised
    #########################################################
    cudavar = 'cuda_visible_devices'
    if cudavar in os.environ:
        cudadev = os.environ[cudavar]
        n_gpu = len(cudadev.split(','))
        print(cudavar, '=', cudadev)
        print("found {} gpu devices".format(n_gpu))
    else:
        n_gpu = 1
    # print on which host this is running (useful for troubleshooting on clusters).
    print("{}: running on host '{}'".format(
        datetime.now().strftime("%Y%m%d-%H%M%S"),
        platform.node()
    ))
    ## set up multi gpu
    btch = n_gpu * 2  # set batch size to the number of gpu's
    if n_gpu > 1: # if multi-gpu parallelisation is supported
        strategy = tf.distribute.mirroredstrategy()
        strategy_scope = strategy.scope
        print("*** replicas:", strategy.num_replicas_in_sync)
        print(strategy)
        assert strategy.num_replicas_in_sync == n_gpu
    else: # otherwise run on a single gpu in serial
        strategy_scope = contextlib.nullcontext
    
    ## load metadata
    datf = args.datf 
    vdatf = args.vdatf
    with open(os.path.join(datf, 'parameters.json')) as json_file:
        par = json.load(json_file)
    imgdim = tuple(par['image_size']) # spatial dimensions of input/output
    if par.get('multifocus', None):
        chan_in = par['multifocus'][0]
    else:
        chan_in = 1 # depth of input data
    chan_out = par['num_classes'] # number of predicted class labels
    print('Input channels: ', chan_in)
    print('Output channels: ', chan_out)

    ## compile neural network
    label_type = par['label']
    if label_type == 'Mask' or label_type == 'Disk':
        netparams = netparams_segment
        if chan_out > 1:
            loss = 'categorical_crossentropy'
        else:
            loss = 'binary_crossentropy'
        output_layer = 'segment' 
        metrics = ['accuracy']
        metric_labels = ['accuracy']
        for i in np.arange(chan_out):
            metrics.append(tf.keras.metrics.Precision(class_id=i, name=f'precision_{i}'))
            metric_labels.append(f'precision_{i}') 
        for i in np.arange(chan_out):
            metrics.append(tf.keras.metrics.Recall(class_id=i, name=f'recall_{i}')) 
            metric_labels.append(f'recall_{i}') 
    elif label_type == 'Exitwave' or label_type == 'Exitwave_no_support':
        netparams = netparams_exitwave
        loss = 'mse'
        output_layer = 'regress'
        metrics=[tf.keras.metrics.RootMeanSquaredError(name='rmse'),
                 tf.keras.metrics.MeanAbsoluteError(name='mae')]
        metric_labels=['rmse', 'mae']
    else:
        raise ValueError(f'Unknown label_type: {label_type}')
    # Regularization
    regul = args.regularize
    if not regul:
        regul = None     # Turn 0.0 into None
    print("L2 regularization:", regul)
    
    with strategy_scope():
        tf.keras.backend.clear_session()
        if args.restart is not None:
            model = tf.keras.models.load_model(
                        outf+'checkpoint-{:04d}'.format(args.restart))
            initial_epoch = args.restart
        else:
            model = get_model((None,None,chan_in),
                               chan_out,
                               output_layer,
                               regul,
                               **netparams)
            initial_epoch = 0
    if args.rmsprop is not None and args.adam is not None:
        raise ValueError("You cannot specify both --rmsprop and --adam")
    if args.rmsprop:
        optim = RMSprop(args.rmsprop)
    elif args.adam is None:
        optim = Adam(amsgrad=True)
    else:
        adamargs = eval(args.adam)
        print("Adam parameters:", type(adamargs), adamargs)
        optim = Adam(**adamargs)
    model.compile(
           optimizer=optim,
           loss=loss,
           metrics=metrics
           )
    model.summary(positions=[.3, .6, .7, 1.])
    inputshape = (1,imgdim[0],imgdim[1],1)
    outputshape = model.compute_output_shape(inputshape) 
    print('Model input shape: ', inputshape)
    print('Model output shape: ', outputshape)
    print('Input to output ratio (batch, x_dim, y_dim, channels): ', np.array(outputshape)/np.array(inputshape))
    sys.stdout.flush()

    #########################################################
    ################### prepare logfile #####################
    ### in the global variables at the top we defined some
    ### metrics which will be used to validate the network
    ### along the way. this is the log file where those
    ### metric values will be saved.
    #########################################################
    t = datetime.now().strftime("%Y%m%d-%H%M%S")
    callbacks = [
        tf.keras.callbacks.ModelCheckpoint(
            filepath=outf+'checkpoint-{epoch:04d}',
            monitor='val_loss',
            save_best_only=True),
        tf.keras.callbacks.CSVLogger(outf+t+'.log')
        ]
    
    #########################################################
    ##################### training ##########################
    ### now finally we load the training data each training
    ### epoch and train the network
    #########################################################
    input_data_dir = datf + '/images_labels'
    data_paths = sorted(
        [
            os.path.join(input_data_dir, fname)
            for fname in os.listdir(input_data_dir)
            if fname.endswith(".npz")
            ])
    input_params_dir = datf + '/tem_params'
    params_paths = sorted(
        [
            os.path.join(input_params_dir, fname)
            for fname in os.listdir(input_params_dir)
            ])
    if len(params_paths) == 1:
        print("WARNING: Only one microscope parameter file found (training).")
        print("  ", params_paths[0])
        print("   Assuming global data.", flush=True)
        params_paths = [params_paths[0]] * len(data_paths)
    print('training data:', len(data_paths), ' samples with augment factor of', args.augment_factor)
    assert len(data_paths) == len(params_paths)
    train_gen = datagenerator(
            btch, imgdim, data_paths, params_paths, chan_in, chan_out,
            par['normalizedistance'], augment=True, augment_factor=args.augment_factor
    )
    
    #########################################################
    ################ load validation data ###################
    ### the input folders have been identified, including the
    ### validation dataset folder, here we load that in.
    ### keras will use this to validate the network after
    ### each training epoch
    #########################################################
    input_data_dir = vdatf + '/images_labels'
    data_paths = sorted(
        [
            os.path.join(input_data_dir, fname)
            for fname in os.listdir(input_data_dir)
            if fname.endswith(".npz")
            ])
    input_params_dir = vdatf + '/tem_params'
    params_paths = sorted(
        [
            os.path.join(input_params_dir, fname)
            for fname in os.listdir(input_params_dir)
            ])
    if len(params_paths) == 1:
        print("WARNING: Only one microscope parameter file found (validation).")
        print("  ", params_paths[0])
        print("   Assuming global data.", flush=True)
        params_paths = [os.path.join(input_params_dir, 'parameters')] * len(data_paths)
    print('validation data:', len(data_paths), ' samples')
    assert len(data_paths) == len(params_paths)
    val_gen = datagenerator(
            btch, imgdim, data_paths, params_paths, chan_in, chan_out,
            par['normalizedistance'],augment=False
    )
    
    epochs=args.epochs
    print('tensorflow v.{}'.format(tf.__version__))
    print("starting timing")
    before = time.time()
    model.fit(
          x=train_gen,
          batch_size=btch,
          epochs=epochs,
          validation_data=val_gen,
          callbacks=callbacks,
          initial_epoch=initial_epoch,
          verbose=2
          )
    totaltime = time.time() - before
    print("time: {} sec  ({} hours)".format(totaltime, totaltime/3600))
    
    keras_file = outf + 'model-0'
    model.save(keras_file)
    #tf.keras.models.save_model(model, keras_file, include_optimizer=False)
    print('saved model to:', keras_file)
    keras_file = outf + 'model-0.h5'
    model.save(keras_file)
    print('saved HDF5 model to:', keras_file)
    score = model.evaluate(
            val_gen,
            verbose=0)
    print('Test evaluation:')
    for i, v in enumerate(score):
        print('    ', model.metrics_names[i], v)

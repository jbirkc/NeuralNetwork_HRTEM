"""validate_histogram.py - create a histogram of the network performance

Usage: python validate_histogram.py testdatafolder modelfolder [stepsubfolder]
"""

import sys
sys.path.insert(0, '../')

#import matplotlib
#matplotlib.use('AGG')

from train_imageepochs import datagenerator
import argparse
import os
import json
import numpy as np
from tensorflow import keras

def my_rmse(label, pred):
    #assert (len(pred.shape) == 3 and pred.shape[2] == 2) or len(pred.shape) == 2  
    assert label.shape == pred.shape
    mse = np.mean((pred - label)**2)
    return np.sqrt(mse)

def my_rmss(label):
    mss = np.mean(label**2)
    return np.sqrt(mss)

def my_mae(label, pred):
    #assert (len(pred.shape) == 3 and pred.shape[2] == 2) or len(pred.shape) == 2  
    assert label.shape == pred.shape
    return np.mean(np.abs(pred - label))

def my_prec_recall_f1(label, pred, threshold=0.5):
    label = label > threshold
    pred = pred > threshold
    # Precision: True positive / (True positive + false positive = all predicted)
    truepositive = np.logical_and(pred, label).sum()
    precision =  truepositive / pred.sum()
    # Recall: True positive / (True positive + false negative = all ground truth)
    recall = truepositive / label.sum()
    F1 = 2 * precision * recall / (precision + recall)
    return precision, recall, F1

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "data",
        help="The pathname of the folder where the test data is placed."
        )
    parser.add_argument(
        "model",
        help="The pathname of the folder where the trained model is placed."
        )
    parser.add_argument(
        "modelstep",
        help="The subfolder with the model - default: 'model-0'.",
        nargs='?',
        default="model-0"
        )
    parser.add_argument(
        "-o", "--output",
        help="Name of output file",
        default=None
        )
    parser.add_argument(
        "--epoch",
        help="Use a specific data epoch (default: 0), for use with training data.",
        default=0,
        type=int
        )
    parser.add_argument(
        "--maximages",
        help="Limit the number of images used.",
        default=None,
        type=int
        )
    args = parser.parse_args()

    btch = 10
    with open(os.path.join(args.data, 'parameters.json'), "rt") as pfile:
        dataparameters = json.load(pfile)

    imgdim = tuple(dataparameters['image_size'])

    try:
        chan_in = dataparameters['multifocus'][0]
    except (KeyError, TypeError):
        chan_in = 1
    chan_out = dataparameters.get('num_classes', 1)
    images_per_epoch = dataparameters['images_per_epoch']
    if args.maximages and args.maximages < images_per_epoch:
        images_per_epoch = args.maximages
    
    #########################################################
    ################ load validation data ###################
    ### the input folders have been identified, including the
    ### validation dataset folder, here we load that in.
    ### keras will use this to validate the network after
    ### each training epoch
    #########################################################
    vinput_data_dir = args.data + '/images_labels'
    vdata_paths = \
    [
        os.path.join(vinput_data_dir, f'image_label_{args.epoch:03d}_{i:04d}.npz')
        for i in range(images_per_epoch)
    ]
    vinput_params_dir = args.data + '/tem_params'
    vparams_paths = \
    [
        os.path.join(vinput_params_dir, f'parameters_{args.epoch:03d}_{i:04d}')
        for i in range(images_per_epoch)
    ]
    print('validation data:', len(vdata_paths), ' samples')
    assert len(vdata_paths) == len(vparams_paths)
    
    val_gen = datagenerator(
            btch, imgdim, vdata_paths, vparams_paths, chan_in, chan_out,
            dataparameters['normalizedistance'], augment=False
    )

    print("\n\nLoading model", flush=True)
    modelname = os.path.join(args.model, args.modelstep)
    model = keras.models.load_model(modelname)
    
    print("\n\nMaking predictions", flush=True)
    predictions = model.predict(val_gen, verbose=1)

    print("Extracting labels ...", flush=True)
    # Make new generator as TensorFlow may have left it partway into a new epoch (batching).
    val_gen = datagenerator(
            btch, imgdim, vdata_paths, vparams_paths, chan_in, chan_out,
            dataparameters['normalizedistance'], augment=False
    )
    labels = np.concatenate([x[1].astype(np.float32, casting='same_kind') for x in val_gen])
    print(len(labels))
    print(labels.shape, labels.dtype)
    print("... done.  Shape:", predictions.shape, predictions.dtype, labels.shape, labels.dtype)
    assert predictions.shape == labels.shape
    print()
    print("Data for two first images (full data set):")
    print("RMSE   MAE  (PRECISION  RECALL  F1)")
    print("Img0:   ", my_rmse(labels[0], predictions[0]), my_mae(labels[0], predictions[0]), my_prec_recall_f1(labels[0], predictions[0]))
    print("Img1:   ", my_rmse(labels[1], predictions[1]), my_mae(labels[1], predictions[1]), my_prec_recall_f1(labels[1], predictions[1]))
    print("Crossed:", my_rmse(labels[0], predictions[1]), my_mae(labels[0], predictions[1]), my_prec_recall_f1(labels[0], predictions[1]))

    print()
    print("Data for two first images (without background):")
    print("RMSE   MAE  (PRECISION  RECALL  F1)")
    print("Img0:   ", my_rmse(labels[0,:,:,:-1], predictions[0,:,:,:-1]), my_mae(labels[0,:,:,:-1], predictions[0,:,:,:-1]), my_prec_recall_f1(labels[0,:,:,:-1], predictions[0,:,:,:-1]))
    print("Img1:   ", my_rmse(labels[1,:,:,:-1], predictions[1,:,:,:-1]), my_mae(labels[1,:,:,:-1], predictions[1,:,:,:-1]), my_prec_recall_f1(labels[1,:,:,:-1], predictions[1,:,:,:-1]))
    print("Crossed:", my_rmse(labels[0,:,:,:-1], predictions[1,:,:,:-1]), my_mae(labels[0,:,:,:-1], predictions[1,:,:,:-1]), my_prec_recall_f1(labels[0,:,:,:-1], predictions[1,:,:,:-1]))

    print()
    print("Calculating", flush=True)

    data = {}
    nclass = labels.shape[-1]

    prefix = ('', 'scrambled_')
    rng = np.random.default_rng()
    
    for p in prefix:
        if p == 'scrambled_':
            # Scramble predictions
            rng.shuffle(predictions, axis=0)
            
        data[p+'rmse'] = np.array( [my_rmse(labels[i], predictions[i]) for i in range(len(labels))] )
        for c in range(nclass):
            data[p+f'rmse_{c}'] =  np.array( [my_rmse(labels[i,:,:,c], predictions[i,:,:,c])
                                                  for i in range(len(labels))] )

        data[p+'rel_rmse'] = np.array( [my_rmse(labels[i], predictions[i]) / my_rmss(labels[i])
                                            for i in range(len(labels))] )
        for c in range(nclass):
            data[p+f'rel_rmse_{c}'] = np.array( [my_rmse(labels[i,:,:,c], predictions[i,:,:,c]) / my_rmss(labels[i,:,:,c])
                                                   for i in range(len(labels))] )
        data[p+'mae'] = np.array( [my_mae(labels[i], predictions[i]) for i in range(len(labels))] )
        for c in range(nclass):
            data[p+f'mae_{c}'] = np.array( [my_mae(labels[i,:,:,c], predictions[i,:,:,c]) for i in range(len(labels))] )
        prf = np.array( [my_prec_recall_f1(labels[i], predictions[i]) for i in range(len(labels))] )
        data[p+'precision'] = prf.T[0]
        data[p+'recall'] = prf.T[1]
        data[p+'f1'] = prf.T[2]
        for c in range(nclass):
            prf = np.array( [my_prec_recall_f1(labels[i,:,:,c], predictions[i,:,:,c]) for i in range(len(labels))] )
            data[p+f'precision_{c}'] = prf.T[0]
            data[p+f'recall_{c}'] = prf.T[1]
            data[p+f'f1_{c}'] = prf.T[2]
            
    if args.output is None:
        output = os.path.join(args.model, f"histogram-epoch-{args.modelstep}.npz")
    else:
        output = os.path.join(args.model, args.output)
        
    np.savez_compressed(output, **data)
    print(f"Result saved to {output}")

    print()
    print("Summary (average, min, max):")
    for label, value in data.items():
        v = value[~np.isnan(value)]
        print("   ", label, v.mean(), v.min(), v.max())
    
if __name__ == "__main__":
    main()
    

import numpy as np
import json
import os
import shutil
import argparse
import multiprocessing
from temnn.imagesimul.makeimages import MakeImages
from temnn.data.loaddata import load
from tqdm import tqdm

if __name__  == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("data_dir", help="Path to directory with exit waves.")
    parser.add_argument("param_file", help="Path to parameters file.")
    parser.add_argument('--train', dest='test', action='store_false',
                       help="Training data? (default)")
    parser.add_argument('--test', dest='test', action='store_true',
                       help="Test data?")
    parser.add_argument("-n", "--numproc", type=int, default=None,
                        help="Number of processes to use (CPU cores).  Default: all available.")
    parser.add_argument('--seed', type=int, default=None,
                        help="Seed for the random number generator.  Overrrules seed in parameter JSON file.")
    parser.set_defaults(train=True, test=False)
    args = parser.parse_args()

    ### *** Output Directories handling ***
    data_dir =  args.data_dir
    if args.test:
        data_dir += '-test'
    else:
        data_dir += '/'
    if not os.path.exists(data_dir):
        os.makedirs(data_dir)
    image_dir = os.path.join(data_dir, 'images_labels')
    if not os.path.exists(image_dir):
        os.makedirs(image_dir)
    params_dir = os.path.join(data_dir, 'tem_params')
    if not os.path.exists(params_dir):
        os.makedirs(params_dir)
    debug_dir = os.path.join(data_dir, 'debug')
    if not os.path.exists(debug_dir):
        os.makedirs(debug_dir)
    
    # Add parameters dictonary elements
    print('{}'.format(args.param_file))
    with open('{}'.format(args.param_file), 'r') as pfile:
        parameters = json.load(pfile)
    parameters['data_dir'] = data_dir
    parameters['debug_dir'] = debug_dir  # Make it accessible to MakeImages

    # Number of processors.
    # Not stored/read from json as not relevant for reproducibility.
    numproc = args.numproc
    if numproc == None:
        numproc = len(os.sched_getaffinity(0))

    # We generate different images for each epoch.  It is not necessary to
    # use all image_epochs when training, and it is possible to reuse
    # them.  In test data, this is overwritten to 1.
    if args.test:
        image_epochs = 1
        parameters['image_epochs'] = image_epochs
    else:
        image_epochs = parameters['image_epochs'] 
    # Extract image size
    #sampling = np.mean(parameters['sampling'])
    
    # Load data as Dataset object
    data = load(data_dir)
    print('Output directory:', data_dir)
    
    # Extract number of waves (images per epoch) 
    n = len(os.listdir(os.path.join(data_dir, 'wave')))
    print("Number of images {} x {} image epochs.".format(
            n, image_epochs), "- Total images: ", n*image_epochs)
    parameters['images_per_epoch'] = n

    # For reproducibility, the seed for the RNG can come from (priorities)
    # 1) The command line.
    # 2) The parameter JSON file
    # 3) Randomly generated
    if args.seed:
        print("Using random seed from command line")
        masterseed = np.random.SeedSequence(parameters['seed'])
    elif 'seed' in parameters:
        print("Using random seed from JSON parameter file")
        masterseed = np.random.SeedSequence(parameters['seed'])
    else:
        print("Generating a new random seed.")
        masterseed = np.random.SeedSequence(None)
    parameters['seed'] = masterseed.entropy
    print("Random seed:", masterseed.entropy)

    # Make a sequence of two seeds, use the first if training, the second if testing.
    actualseed = masterseed.spawn(2)[args.test]

    # Keep a copy of this script for reference ...
    shutil.copy2(__file__, data_dir)
    
    ### *** Generate image stream *** 
    multiprocessing.set_start_method('spawn')     # For abTEM compatability
    imagestream = MakeImages(data,
                             parameters,
                             label=parameters['label'],
                             seed=actualseed,
                             maxcpu=numproc)
    if 'randomvectorlength' not in parameters:
        parameters['randomvectorlength'] = imagestream.randomvectorlength
        
    ## *** Output ***
    
    # Also store the parameters in a machine_readable file
    with open(os.path.join(data_dir, "parameters.json"), "wt") as pfile:
        json.dump(parameters, pfile, sort_keys=True, indent=4)
    
    if parameters['label'] == 'Mask' or parameters['label'] == 'Disk':
        dtype = 'int8'
    else:
        dtype = 'float32'
    i = 0
    for epoch in range(image_epochs):
        print("Image epoch: {}/{}".format(epoch+1,image_epochs))
        imagelabelfile = os.path.join(image_dir,
                            f'image_label_{epoch:03d}_{{:04d}}')
        temparamfile = os.path.join(params_dir,
                        f"parameters_{epoch:03d}_{{:04d}}")
        #print("    Images/labels:", imagelabelfile)
        #print("    TEM parameters:", temparamfile)
        nn = imagestream.write_all_examples(imagelabelfile, temparamfile)
        assert(n == nn)

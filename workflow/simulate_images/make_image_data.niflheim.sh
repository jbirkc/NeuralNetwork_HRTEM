#!/bin/bash
#SBATCH --job-name=MoS2-images
#SBATCH --mail-type=START,END
#SBATCH --partition=xeon40
#SBATCH --output=slurm-%x-%j.out
#SBATCH --time=24:00:00
#SBATCH -N 1
#SBATCH -n 40

# Necessary modules
source ../../../venv/bin/activate

export OMP_NUM_THREADS=1

python make_image_data.py ../simulation_data/MoS2_supported parameters_mos2_exitwave.json --train
python make_image_data.py ../simulation_data/MoS2_supported parameters_mos2_exitwave.json --test

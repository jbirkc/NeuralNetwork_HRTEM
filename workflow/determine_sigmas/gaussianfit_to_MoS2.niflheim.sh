#!/bin/bash
#SBATCH --job-name=gaussfit-test
#SBATCH --mail-type=FAIL,END
#SBATCH --partition=sm3090
#SBATCH --output=slurm-%x-%j.out
#SBATCH --time=3:30:00
#SBATCH -N 1
#SBATCH -n 8
#SBATCH --gres=gpu:RTX3090:1

# Necessary modules
source $HOME/bachelor/bachelor-venv/bin/activate
# 

python gaussianfit_to_MoS2.py ../simulation_data/test_params_MoS2_layers_8-test/ ../trained_networks/test_params_MoS2_layers_8/

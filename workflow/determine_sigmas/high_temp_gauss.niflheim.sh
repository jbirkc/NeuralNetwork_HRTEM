#!/bin/bash
#SBATCH --job-name=Fit-high-temp
#SBATCH --mail-type=FAIL,END
#SBATCH --partition=sm3090
#SBATCH --output=slurm-%x-%j.out
#SBATCH --time=3:30:00
#SBATCH -N 1
#SBATCH -n 8
#SBATCH --gres=gpu:RTX3090:1

# Necessary modules
source $HOME/bachelor/bachelor-venv/bin/activate
# 

python gaussianfit_to_MoS2.backup.py ../simulation_data/experimental_data_high_temp-test/ ../trained_networks/experimental_data_high_temp/

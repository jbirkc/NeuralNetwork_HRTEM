import sys
import os
import json
import argparse
import numpy as np
# sys.path.insert(0, '../')

import tensorflow.keras as keras
from temnn.data.mods import local_normalize
# from peakfitting.peaks import find_local_peaks, refine_peaks
from peakfitting.fit import find_local_peaks, refine_peaks

def rmse(a):
    return np.sqrt(np.mean(a*a))

def create_output_folder(datf):
    ## Create output folder ##
    if not datf.endswith("/"):
        out_dir = datf + "/"
    out_dir = datf + 'fitting_parameters/'
    
    if not os.path.exists(out_dir):
        print("Creating ", out_dir)
        os.makedirs(out_dir)
    return out_dir

def determine_sigma(arr, region=11):
    _ = arr[..., 0].squeeze() # real values
    imaginary_values = arr[..., 1].squeeze()

    peaks = find_local_peaks(image=imaginary_values,
                             min_distance=10,
                             threshold=0.30,
                             exclude_border=10,
                             exclude_adjacent=True)

    popt, messages = refine_peaks(image=imaginary_values,
                                  points=peaks)
    points, z, A, sigma = popt
    return sigma, peaks


def main(datf, net):
    
    ############# model_path = os.path.join(net, 'model-0')
    ############# print('Loading model', model_path)
    
    ############# model = keras.models.load_model(model_path)
    
    # Get amount of files in tem_params for loop
    N_systems = len(os.listdir(datf + "tem_params/"))
    
    print("### Predicting and calculating sigma")
    parameters = json.load(open(os.path.join(datf, "parameters.json")))
    
    # Get image size
    image_size = parameters['image_size']
    
    # # Pre-allocate memory
    # sigma_prediction = np.empty(shape=(N_systems, *image_size, 2))
    # sigma_groundTruth = np.empty(shape=(N_systems, *image_size, 2))
    # temp_list = np.empty(shape=(N_systems, 1))
    
    model_path = os.path.join(net, "model-0")
    print("CNN is: ", model_path, flush=True)
    model = keras.models.load_model(model_path)
    
    temp_list = []
    layers_list = []
    sigma_prediction = []
    sigma_groundTruth = []
    peaks_groundTruth = []
    peaks_prediction = []
    
    # Determine ground truth and pre
    for n in range(N_systems):
        imagenumber = "{:04d}".format(n)
        try:
            imageparameters = json.load(open(os.path.join(datf, "tem_params", "parameters_000_" + imagenumber+'.json')))
        except FileNotFoundError:
            continue
        print(f"Iteration {n}", flush=True)
        tmp = np.load(os.path.join(datf, 'images_labels', 'image_label_000_'+imagenumber+'.npz'))
        image = tmp['image']
        wave = tmp['label']
        model_properties = json.load(open(os.path.join(datf, "model_properties", "model_properties_"+imagenumber+".json")))
        layers_list.append(model_properties["graphitelayers"])
        
        # Save temperature
        try:
            # temp_list[n] = imageparameters['Temp']
            temp_list.append(imageparameters["Temp"])
        except KeyError:
            print("`Temp` is not a file in the archive.\n Will return temp_list = [False, False, ..., False]")
            # temp_list[n] = False
            temp_list.append([None])
        
        
        
        
        sampling = imageparameters["sampling"]
        normalizedistance = parameters["normalizedistance"] / sampling
        
        print("Normalizing")
        normimage = local_normalize(image.copy(), normalizedistance, normalizedistance)
        print("Predicting")
        prediction = model.predict(normimage)
        
        print("Saving sigmas and peaks")
        
        sigmas_gt, peaks_gt = determine_sigma(wave, region=11)
        sigma_groundTruth.append(sigmas_gt)
        peaks_groundTruth.append(peaks_gt)
        
        sigma_pred, peaks_pred = determine_sigma(prediction, region=11)
        sigma_prediction.append(sigma_pred)
        peaks_prediction.append(peaks_pred)
        ############### sigma_prediction.append(determine_sigma(prediction, region=11))
        
        
    temp_list = np.asarray(temp_list, dtype=object).squeeze()
    sigma_prediction = np.asarray(sigma_prediction, dtype=object).squeeze()
    sigma_groundTruth  = np.asarray(sigma_groundTruth, dtype=object).squeeze()
    peaks_groundTruth = np.asarray(peaks_groundTruth, dtype=object).squeeze()
    layers_list = np.asarray(layers_list, dtype=object).squeeze()
    return temp_list, layers_list, sigma_prediction, sigma_groundTruth, peaks_prediction, peaks_groundTruth

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    positionalparams = ("datf", "network"
                        # ,"outf"
                        )
    parser.add_argument("datf", 
                        help="The path and name of the folder where the test data is placed.")
    parser.add_argument("network", 
                        help="The path and name of the folder where the trained network is placed.")
    # parser.add_argument("outf",
    #                     help="The path and name of the folder where the output is placed.")
    
    args = parser.parse_args()
    
    ## Create output folder ##
    datf = args.datf
    net = args.network
    
    temp, layers, sigmas_pred, sigmas_gt, peaks_pred, peaks_gt = main(datf, net)
    
    fpath = create_output_folder(datf)
    print(f"Output directory:\t {fpath}", flush=True)
    
    fname = fpath + "fit_params"
    np.savez(fname, 
             temp=temp, layers=layers,
             sigmas_pred=sigmas_pred, 
             sigmas_gt=sigmas_gt, 
             peaks_pred=peaks_pred, peaks_gt=peaks_gt)

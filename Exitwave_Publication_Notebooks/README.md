
Reproducing figures from the exit wave publication
==================================================


Figures in the manuscript are produced by these notebooks.  The entries list the name of the produced image file, the name of the Notebook, the dataset used and the neural network used.  Instructions on how to reproduce the datasets appear below.


Figure 1
--------

* Figure: MoS2_unsup_0608_wide.png
* Notebook: MakeFigure_MoS2_unsup_inset.ipynb
* Data: c2db_15pm-test
* Net: c2db_15pm


Figure 2
--------

* Figure: MoS2_supported_0367_wide.png
* Notebook: MakeFigure_MoS2_supported_inset.ipynb
* Data: MoS2_supported_15pm-test
* Net: MoS2_supported_15pm


Figure 3
--------

* Figure: c2db_1132_Cl2Co2_RMSE0.0263_wide.png
* Notebook: MakeFigure_c2db_inset.ipynb
* Data: c2db_15pm-test
* Network: c2db_15pm


Figure 4:
---------

* Figure: rmse_types.png
* Notebook: RMSE_Histograms.ipynb
* Data: Histogram data stored with trained networks
* Networks: MoS2_15pm, MoS2_supported_15pm, c2db_15pm


Figure 5 panel a, b, c
----------------------

* Figure:   Argand_MoS2unsup_0608.png
* Notebook: ArgandPlot_MoS2_unsup.ipynb
* Data:     MoS2_15pm-test
* Network:  MoS2_15pm


Figure 5 panel d, e, f
----------------------

* Figure:   Argand_MoS2_supported_0367.png
* Notebook: ArgandPlot_MoS2_supported.ipynb
* Data:     MoS2_supported_15pm-test
* Network:  MoS2_supported_15pm


Figure 6:
---------

* Figure:   rmse_nimages.png
* Notebook: RMSE_Histograms.ipynb (same as Figure 4)
* Data:	    Histogram data stored with trained networks
* Networks: img1_15pm, img2_15pm, MoS2_supported_15pm, img4_15pm


Figures 7, 8, S5, S6, S7 and S8
-------------------------------

These figures are generated from the same notebook, but with different systems chosen in the block with the title **Load image**.  For the first two figures, set the variable ``percent`` to -1, and ``number`` to the number given below.  For the supplementary figures, unset ``number`` and set ``percent`` to the values below.

* Figures:
  - c2db_0524_AgCuTe2_RMSE0_0301.png (fig. 7, number = 524)
  - c2db_1543_Cl2Pt2Se2_RMSE0_0308.png (fig. 8, number = 1543)
  - c2db_0091_F2Si2_RMSE0_0337_5percent.png (fig. S5, percent = 5)
  - c2db_1285_GeO2_RMSE0_0231_25percent.png (fig. S6, percent = 25)
  - c2db_0759_Br2Nb_RMSE0_0314_75percent.png (fig. S7, percent = 75)
  - c2db_1794_Br2Co_RMSE0_0104_95percent.png (fig. S8, percent = 95)
* Notebook: MakeFigure_c2db_supplementary.ipynb
* Data: c2db_15pm-test
* Network: c2db_15pm


Figure 9
--------

* Figure: MoS2_Experimental_10_19_28.png
* Notebook: AnalyseExperimentalMoS2.ipynb
* Data: File FFS6b_Def_-1nm.dm3 courtesy the authors of Ref. 17.
* Network: MoS2_supported_lores_15pm


Figures 10, S13, S14
--------------------

The system is selected by setting the variable ``system`` in the cell with the title *Select system*.

* Figures:
  - ReconstructionComparison_small_862.png (Fig. 10, system = 862)
  - ReconstructionComparison_small_367.png (Fig. S13, system = 367)
  - ReconstructionComparison_small_265.png (Fig. S14, system = 265)
* Notebook: MoS2_Tempas_Exitwave.ipynb
* MoS2_supported_reconstruct_15pm-test plus reconstructions of the exit waves with MacTempas
* Network: MoS2_supported_15pm


Figure S1
---------

Produced in Powerpoint by the authors.



Figure S2
---------

* Figure: figS2_Multislice_convergence.png
* Notebook: Convergence_Multislice_MoS2.ipynb
* Data: -
* Network: -


Figure S3
---------

* Figure: trainingcurve.png 
* Notebook: LearningCurves_HistoData.ipynb
* Data: Histogram data stored with the trained networks
* Networks: 
  - Panel a: 
    - MoS2_15pm-Nx where x is 10, 25, 50, 100, 250, 500
    - MoS2_15pm
    - c2db_15pm-Nx where x is 10, 25, 50, 100, 250, 500, 1000, 2500
    - c2db_15pm
  - Panel b: MoS2_15pm, MoS2_supported_15pm, c2db_15pm


Figure S4
---------

* Figure: EffectOfSmear0367.png
* Notebook: MakeFigure_EffectOfSmearing.ipynb
* Data: MoS2_supported-test and MoS2_supported_15pm-test
* Netowrk: -


Figures S9, S10, S11, S12
-------------------------

* Figures:
  - figS9_MoS2@C_0278_RMSE0_0189_5percent  (Fig. S9, percent = 5)
  - figS10_MoS2@C_0862_RMSE0_0148_25percent  (Fig. S10, percent = 25)
  - figS11_MoS2@C_0265_RMSE0_0145_75percent  (Fig. S11, percent = 75)
  - figS12_MoS2@C_0283_RMSE0_0104_95percent  (Fig. S12, percent = 95)
* Notebook: MakeFigure_MoS2_supplementary.ipynb
* Data: MoS2_supported_15pm-test
* Network: MoS2_supported_15pm



Reproducing the data
====================

The data required to reproduce the data in the publication is
available from doi:10.11583/DTU.15263655

Download the file and unpack it.  It contains:

* A copy of these instructions (README.md)
* A folder ``scripts`` with scripts for reproducing the data
* A folder ``trained_models`` with the trained models.

The code can be downloaded by cloning the gitlab repository at
https://gitlab.com/schiotz/NeuralNetwork_HRTEM and then switching to
the branch ExitWave:

    git clone https://gitlab.com/schiotz/NeuralNetwork_HRTEM.git
    cd NeuralNetwork_HRTEM
    git checkout -t origin/ExitWave

or by downloading the release Exitwave_reconstruction_June_2022 from
https://gitlab.com/schiotz/NeuralNetwork_HRTEM/-/releases/Exitwave_reconstruction_June_2022

To use it, you need a Python installation with TensorFlow 2.5,
ASE 3.22 and abTEM.  The version of ASE and abTEM should be
uncritical, TensorFlow may cause trouble if you use a later version,
as they tend to change the API occationally.  See
https://gitlab.com/schiotz/NeuralNetwork_HRTEM for installation
instructions.


Reproducing the unsupported MoS2 dataset and the corresponding model
--------------------------------------------------------------------

1. Reproducing the structures

   The folder ``scripts/MoS2`` contains a script
   ``reproduce_waves.sh``.  Copy it to the folder
   ``NeuralNetwork_HRTEM/workflow/simulate_waves/simulate_2Dmaterials``.

   The file contains the python command that reproduces the structures
   for the unsupported MOS2 training data set.  The python script is
   called with a number of arguments aimed at producing exactly the
   same result, including a ``--seed`` argument seeding the random
   number generator, and a ``--numproc`` argument setting the number
   of parallel processes used when generating structures.  To
   reproduce the same structures, neither must be changed.  If your
   computer has more or less than 24 cores, the script will over- or
   undersubscribe your CPU cores, that may hurt performance a bit but
   is otherwise not problematic.  The output will appear in the folder
   ``NeuralNetwork_HRTEM/workflow/simulation_data/MoS2``. 

   Do the same with the similar script in
   ``scripts/MoS2-test`` to produce the test data.

2. Reproducing the images

   The folder ``scripts/MoS2`` contains a file
   ``parameters.json`` with the microscope paramters.  Copy it to the
   folder ``NeuralNetwork_HRTEM/workflow/simulate_images`` and name it
   ``parameters_train.json``.  In that
   folder, run the following script (or submit it to a computer
   cluster)

       python make_image_data.py ../simulation_data/MoS2 parameters_train.json --train

   Note that this will run on all available cores.  You can specify
   the number of cores with ``-n 10``, for example.  Unlike the script
   that generates the structures, the result will not depend on the
   number of cores used, as all random numbers are generated on the
   master.  The seed for the random number generator is in the json
   file.

   You now need to copy the similar ``parameters.json`` file from
   ``scripts/MoS2-test``, name it ``parameters_test.json`` and run
   the command

       python make_image_data.py ../simulation_data/MoS2 parameters_test.json --test

   It is unfortunate that the random number seed was different in when
   producing the training and test set, as that makes it necessary to
   use different json files (the script was made to generate different
   sequences from the same seed depending on whether ``--train`` or
   ``--test`` was specified).


3. Smoothen the exit waves used for training.  Run the script

       python smearwave.py ../simulation_data/MoS2 15 
       python smearwave.py ../simulation_data/MoS2-test 15 

   This will fold the exit wave with a 15 pm Gaussian.  The result
   appears in the folders ``../simulation_data/MoS2_15pm`` and
   ``../simulation_data/MoS2_15pm-test``.

4. Train the networks

   Go to the folder
   ``NeuralNetwork_HRTEM/workflow/train_simulation/Unet`` and run the
   python scripts

       python train_imageepochs.py 200 ../../simulation_data/MoS2_15pm ../../simulation_data/MoS2_15pm-test  ../../trained_networks/MoS2_15pm --rmsprop=5e-4
       python validate_histogram.py ../../simulation_data/MoS2_15pm-test ../../trained_networks/MoS2_15pm

   The first line trains the network (requires a GPU!).  The second
   generates evaluation data used by various notebooks.

   **Alternative:** Find the trained network in the supplied
   ``trained_networks`` folder.


Reproducing the supported MoS2 dataset and the corresponding model
------------------------------------------------------------------

Follow the same instructions as above, but find the files in the
``scripts/MoS2_supported`` and
``scripts/MoS2_supported-test``.


Reproducing the C2DB dataset and the corresponding model
--------------------------------------------------------

You will need a copy of the C2DB database, owned by the group of0
prof. Kristian Thygesen at DTU Physics.  The database is found here:
https://cmr.fysik.dtu.dk/c2db/c2db.html or as an alternate link here:
https://c2db.fysik.dtu.dk and the data can be obtained as an ASE
database file by contacting prof.
[Thygesen](https://www.dtu.dk/person/kristian-sommer-thygesen?id=13069)

You should get the version from 24 June 2021 if you want an exact
reproduction of our results.

1. Reproducing the structures and wavefunctions

   Like in the previous cases, the folder ``scripts/c2db``
   contains a script ``reproduce.sh`` that reproduces the data.  It
   should be copied to the same
   ``workflow/simulate_waves/simulate_2Dmaterials`` as used for MoS2.
   Unlike in the previous cases, it produces both the training and
   testing data together.

The following steps are identical to the MoS2 cases.


Reproducing the test for number of images (Figure 6)
----------------------------------------------------

The same wave functions should be used as for MoS2_supported. You can
obtain this in one of the following ways (provided you have already
produced the MoS2_supported data)

* Make folder ``img1``, ``img2`` and ``img4`` in ``simulation_data`` similar to
  MoS2_supported, and make softlinks to the folders ``model``,
  ``points``, and ``wave`` in ``MoS2_supported``.

**OR**

* Copy those folders from ``simulation_data/MoS2_supported`` to three
so far identical folders ``img1``, ``img2`` and ``img4``.

**OR**

* Rerun the script creating ``MoS2_supported`` three times with the
  same seed but with different output folders.

Then do the same with ``img1-test``, ``img2-test``, ``img4-test``,
based off ``MoS2_supported-test``.

Then proceed with steps 2 - 4 above.


